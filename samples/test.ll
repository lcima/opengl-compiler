; ModuleID = 'samples/test.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-redhat-linux-gnu"

@x = global i32 0

define i32 @test(i32 %arg0, i32 %arg1) {
entry:
  %f = alloca i32
  store i32 %arg0, i32* %f
  %r = alloca i32
  store i32 %arg1, i32* %r
  %y = alloca i32
  %z = alloca i32
  %0 = load i32* %z
  store i32 0, i32* %z
  %1 = load i32* %y
  store i32 0, i32* %y
  %2 = load i32* %z
  %3 = icmp slt i32 %2, 3
  br i1 %3, label %while, label %while_footer

while_footer:                                     ; preds = %while, %entry
  ret i32 10

while:                                            ; preds = %while, %entry
  %d = alloca i32
  %4 = load i32* %d
  store i32 5, i32* %d
  %5 = load i32* %z
  %6 = icmp slt i32 %5, 3
  br i1 %6, label %while, label %while_footer
}
