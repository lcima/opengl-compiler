/* File: ast_expr.cc
 * -----------------
 * Implementation of expression node classes.
 */

#include <string.h>
#include "ast_expr.h"
#include "ast_type.h"
#include "ast_decl.h"
#include <iostream>
#include <vector>

struct cmp_str
{
   bool operator()(char const *a, char const *b)
   {
      return std::strcmp(a, b) < 0;
   }
};

llvm::Value* ArithmeticExpr::Emit() {
	llvm::Value* lVal = NULL;
	llvm::Value* baseL = NULL;
	llvm::Value* baseR = NULL;
	char* swizzleL = NULL;
	char* swizzleR = NULL;
	llvm::Constant* lidxL = NULL;
	llvm::Constant* lidxR = NULL;
	int LSize = 0;
	int RSize = 0;
	llvm::Constant* maskL = NULL;
	llvm::Constant* maskR = NULL;

	if(left != NULL) {
		lVal = left->Emit();
		if(dynamic_cast<VarExpr*>(left) || dynamic_cast<AssignExpr*>(left)) {
			//if (!dynamic_cast<llvm::Argument*>(lVal))
				lVal = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
		}
		else if(dynamic_cast<FieldAccess*>(left)) {
			baseL = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
			swizzleL = dynamic_cast<FieldAccess*>(left)->getSwizzle();
			LSize = strlen(swizzleL);
			if(LSize == 1) {
				int idx = -1;
				if(!strcmp(swizzleL,"x"))
					idx = 0;
				else if(!strcmp(swizzleL,"y"))
					idx = 1;
				else if(!strcmp(swizzleL,"z"))
					idx = 2;
				else if(!strcmp(swizzleL,"w"))
					idx = 3;
				lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
				lVal = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
			}
			else {
				std::vector<llvm::Constant *> indices;
				for(int i = 0; i<strlen(swizzleL); i++) {
					char letter = swizzleL[i];
					if(letter == 'x') {
						llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 0);
						indices.push_back(maskVal);
					}
					else if(letter == 'y') {
						llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 1);
						indices.push_back(maskVal);
					}
					else if(letter == 'z') {
						llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 2);
						indices.push_back(maskVal);
					}
					else if(letter == 'w') {
						llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 3);
						indices.push_back(maskVal);
					}
				}
				maskL = llvm::ConstantVector::get(indices);
				lVal = new llvm::ShuffleVectorInst(baseL, llvm::UndefValue::get(baseL->getType()), maskL, "", irgen.GetBasicBlock());
			}
		}
	}
	llvm::Value* rVal = right->Emit();
	llvm::Value* rLoc = rVal;
	if(dynamic_cast<VarExpr*>(right) || dynamic_cast<AssignExpr*>(right)) {
		//if(!dynamic_cast<llvm::Argument*>(rVal))
			rVal = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(right)) {
		baseR = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
		swizzleR = dynamic_cast<FieldAccess*>(right)->getSwizzle();
		RSize = strlen(swizzleR);
		if(RSize == 1) {
			int idx = -1;
			if(!strcmp(swizzleR,"x"))
				idx = 0;
			else if(!strcmp(swizzleR,"y"))
				idx = 1;
			else if(!strcmp(swizzleR,"z"))
				idx = 2;
			else if(!strcmp(swizzleR,"w"))
				idx = 3;
			lidxR = llvm::ConstantInt::get(irgen.GetIntType(), idx);
			rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
		}
		else {
			std::vector<llvm::Constant *> indices;
			for(int i = 0; i<strlen(swizzleR); i++) {
				char letter = swizzleR[i];
					if(letter == 'x') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 0);
					indices.push_back(maskVal);
				}
				else if(letter == 'y') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 1);
					indices.push_back(maskVal);
				}
				else if(letter == 'z') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 2);
					indices.push_back(maskVal);
				}
				else if(letter == 'w') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 3);
					indices.push_back(maskVal);
				}
			}
			maskR = llvm::ConstantVector::get(indices);
			rVal = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), maskR, "", irgen.GetBasicBlock());
		}
	}
	char* op_ = op->getOp();

	// Unary
	if(lVal == NULL) {
		if(!strcmp(op_, "++")) {
			llvm::Value *val;
			if(irgen.isVector(rVal) != 0) {
				val = irgen.makeVector(1.0, irgen.isVector(rVal));
			}
			else if(irgen.isInt(rVal)) {
				val = llvm::ConstantInt::get(irgen.GetIntType(), 1);
			}
			else if(irgen.isFloat(rVal)) {
				val = llvm::ConstantFP::get(irgen.GetFloatType(), 1);
			}
			llvm::Value *sum = llvm::BinaryOperator::CreateAdd(rVal, val, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, rLoc, irgen.GetBasicBlock());
			rVal = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
			return rVal;
		}
		if(!strcmp(op_, "--")) {
			llvm::Value *val;
			if(irgen.isVector(rVal) != 0) {
				val = irgen.makeVector(1.0, irgen.isVector(rVal));
			}
			else if(irgen.isInt(rVal)) {
				val = llvm::ConstantInt::get(irgen.GetIntType(), 1);
			}
			else if(irgen.isFloat(rVal)) {
				val = llvm::ConstantFP::get(irgen.GetFloatType(), 1);
			}
			llvm::Value *sum = llvm::BinaryOperator::CreateSub(rVal, val, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, rLoc, irgen.GetBasicBlock());
			rVal = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
			return rVal;
		}
		if(!strcmp(op_, "+")) {
			return rVal;
		}
		if(!strcmp(op_, "-")) {
			llvm::Value *val = llvm::BinaryOperator::CreateNeg(rVal, "", irgen.GetBasicBlock());
			return val;
		}
	}
	// Regular
	else {
		// Left is vec right is not
		/*if(irgen.isVector(lVal) != 0 && irgen.isVector(rVal) == 0) {
			if(irgen.isFloat(rVal)) {
fprintf(stderr, "here");
				llvm::Constant* val = (llvm::Constant*)rVal;
			}

			std::vector<llvm::Constant *> indices;
			for(int i = 0; i < irgen.isVector(lVal); i++) {
				llvm::Constant *maskVal = llvm::ConstantFP::get(irgen.GetFloatType(), 0);
				indices.push_back(maskVal);
			}
			llvm::Constant* mask = llvm::ConstantVector::get(indices);
			rLoc = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
			rVal = new llvm::ShuffleVectorInst(rLoc, llvm::UndefValue::get(rLoc->getType()), mask, "", irgen.GetBasicBlock());
		}*/
		if(!strcmp(op_, "+")) {
			llvm::Value *sum = llvm::BinaryOperator::CreateAdd(lVal, rVal, "", irgen.GetBasicBlock());
			return sum;
		}
		if(!strcmp(op_, "-")) {
			llvm::Value *sum = llvm::BinaryOperator::CreateSub(lVal, rVal, "", irgen.GetBasicBlock());
			return sum;
		}
		if(!strcmp(op_, "*")) {
			llvm::Value *sum = llvm::BinaryOperator::CreateMul(lVal, rVal, "", irgen.GetBasicBlock());
			return sum;
		}
		if(!strcmp(op_, "/")) {
			llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(lVal, rVal, "", irgen.GetBasicBlock());
			return sum;
		}
	}

	return NULL;
}

llvm::Value* AssignExpr::Emit() {
	extern List< std::map<char*, llvm::Value*, cmp_str>* >* scope_stack;

	llvm::Value* lVal = left->Emit();
	llvm::Value* rVal = right->Emit();
	llvm::Value* rLoc = NULL;

	// For field selection
	llvm::Value* baseL = NULL;
	llvm::Value* baseR = NULL;
	char* swizzleL = NULL;
	char* swizzleR = NULL;
	llvm::Constant* lidxL = NULL;
	llvm::Constant* lidxR = NULL;
	int LSize = 1;
	int RSize = 1;
	llvm::Constant *mask = NULL;
	std::vector<llvm::Constant *> indices;

	if(dynamic_cast<FieldAccess*>(left)) {
		baseL = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
		swizzleL = dynamic_cast<FieldAccess*>(left)->getSwizzle();
		if(strlen(swizzleL) > 1) {
			LSize = strlen(swizzleL);
		}
		else {
			int idx = -1;
			if(!strcmp(swizzleL,"x"))
				idx = 0;
			else if(!strcmp(swizzleL,"y"))
				idx = 1;
			else if(!strcmp(swizzleL,"z"))
				idx = 2;
			else if(!strcmp(swizzleL,"w"))
				idx = 3;
			lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		}
	}
	if(dynamic_cast<FieldAccess*>(right)) {
		baseR = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
		swizzleR = dynamic_cast<FieldAccess*>(right)->getSwizzle();
		RSize = strlen(swizzleR);
		if(RSize > 1) {
			for(int i = 0; i<strlen(swizzleR); i++) {
				char letter = swizzleR[i];
				if(letter == 'x') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 0);
					indices.push_back(maskVal);
				}
				else if(letter == 'y') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 1);
					indices.push_back(maskVal);
				}
				else if(letter == 'z') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 2);
					indices.push_back(maskVal);
				}
				else if(letter == 'w') {
					llvm::Constant *maskVal = llvm::ConstantInt::get(irgen.GetIntType(), 3);
					indices.push_back(maskVal);
				}
			}
			mask = llvm::ConstantVector::get(indices);
		}
		else {
			int idx = -1;
			if(!strcmp(swizzleR,"x"))
				idx = 0;
			else if(!strcmp(swizzleR,"y"))
				idx = 1;
			else if(!strcmp(swizzleR,"z"))
				idx = 2;
			else if(!strcmp(swizzleR,"w"))
				idx = 3;
			lidxR = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		}
	}

	char* op_ = op->getOp();
	std::map<char*,llvm::Value*, cmp_str>::const_iterator it;
	int count = scope_stack->NumElements() - 2;
	std::map<char*, llvm::Value*, cmp_str>* symTable = scope_stack->top();
	char *cstr = new char [lVal->getName().str().length() + 1];
	strcpy(cstr, lVal->getName().str().c_str());
	it = symTable->find(cstr);
	delete [] cstr;
	while(it == symTable->end() && count >= 0) {
		symTable = scope_stack->Nth(count);
		char *cstr = new char[lVal->getName().str().length() + 1];
		strcpy(cstr, lVal->getName().str().c_str());
		it = symTable->find(cstr);
		delete [] cstr;
		count = count-1;
	}
	llvm::Value* varName = 	it->second;
	if(dynamic_cast<VarExpr*>(left) || dynamic_cast<AssignExpr*>(left)) {
		//if(!dynamic_cast<llvm::Argument*>(lVal)) {
			lVal = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
		//}
		//else {
			/*char *n = new char[lVal->getName().str().length() + 1];
			strcpy(n, lVal->getName().str().c_str());
fprintf(stderr, "Adding variable %s\n", n);
			llvm::Type* var = lVal->getType();
			char* f = "f";
			llvm::Value* alloc = new llvm::AllocaInst(irgen.GetIntType(), f,irgen.GetBasicBlock());
			std::map<char*,llvm::Value*, cmp_str>::iterator itr;
			itr = symTable->find(n);
if(itr == symTable->end()) {
fprintf(stderr, "wtf");
}
			symTable->erase(f);

			symTable->insert(std::pair<char*,llvm::Value*>(f,alloc));
			//lVal = new llvm::LoadInst(alloc, "", irgen.GetBasicBlock());
			//llvm::StoreInst *ptr = new llvm::StoreInst(lVal, alloc, irgen.GetBasicBlock());
			//lVal = left->Emit();
			//lVal = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
			return lVal;*/
		//}
	}
	rLoc = rVal;
	if(dynamic_cast<VarExpr*>(right) || dynamic_cast<AssignExpr*>(right)) {
		//if(!dynamic_cast<llvm::Argument*>(rVal))
			rVal = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
	}
	/*else if(dynamic_cast<FieldAccess*>(right)) {
		if(RSize == 1) {
			rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
		}
	}*/
	if(!strcmp(op_,"=")) {
		// Swizzle on both sides
		if(dynamic_cast<FieldAccess*>(left) && dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, rVal, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				for(int i = 0; i<RSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(vecPtr, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					baseL = llvm::InsertElementInst::Create(baseL, rVal, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
				return varName;
			}
		}
		// Swizzle on right side
		if(dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(rVal, varName, irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(vecPtr, varName, "", irgen.GetBasicBlock());
				return varName;
			}
		}
		// Swizzle on left side
		if(dynamic_cast<FieldAccess*>(left)) {
			if(LSize == 1) {
				baseL = llvm::InsertElementInst::Create(baseL, rVal, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				baseR = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
				for(int i = 0; i<LSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(baseR, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					baseL = llvm::InsertElementInst::Create(baseL, rVal, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
			}
		}
		// No swizzles
		else {
			llvm::StoreInst *ptr = new llvm::StoreInst(rVal, varName, irgen.GetBasicBlock());
			return varName;
		}
	}
	if(!strcmp(op_, "+=")){
		// Swizzle on both sides
		if(dynamic_cast<FieldAccess*>(left) && dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateAdd(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				for(int i = 0; i<RSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(vecPtr, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateAdd(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
				return varName;
			}
		}
		// Swizzle on right side
		if(dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateAdd(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateAdd(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(vecPtr, varName, "", irgen.GetBasicBlock());
				return varName;
			}
		}
		// Swizzle on left side
		if(dynamic_cast<FieldAccess*>(left)) {
			if(LSize == 1) {
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateAdd(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				baseR = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
				for(int i = 0; i<LSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(baseR, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateAdd(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
			}
		}
		// No swizzles
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateAdd(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}
		/*if(dynamic_cast<FieldAccess*>(left)) {
			llvm::Value* lField = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
			llvm::Value *sum = llvm::BinaryOperator::CreateAdd(lField, rVal, "", irgen.GetBasicBlock());
			baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
			return varName;
		}
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateAdd(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}*/
	}
	if(!strcmp(op_, "-=")){
		// Swizzle on both sides
		if(dynamic_cast<FieldAccess*>(left) && dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSub(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				for(int i = 0; i<RSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(vecPtr, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateSub(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
				return varName;
			}
		}
		// Swizzle on right side
		if(dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSub(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSub(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(vecPtr, varName, "", irgen.GetBasicBlock());
				return varName;
			}
		}
		// Swizzle on left side
		if(dynamic_cast<FieldAccess*>(left)) {
			if(LSize == 1) {
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSub(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				baseR = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
				for(int i = 0; i<LSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(baseR, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateSub(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
			}
		}
		// No swizzles
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateSub(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}
		/*if(dynamic_cast<FieldAccess*>(left)) {
			llvm::Value* lField = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
			llvm::Value *sum = llvm::BinaryOperator::CreateSub(lField, rVal, "", irgen.GetBasicBlock());
			baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
			return varName;
		}
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateSub(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}*/
	}
	if(!strcmp(op_, "*=")){
		// Swizzle on both sides
		if(dynamic_cast<FieldAccess*>(left) && dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateMul(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				for(int i = 0; i<RSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(vecPtr, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateMul(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
				return varName;
			}
		}
		// Swizzle on right side
		if(dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateMul(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateMul(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(vecPtr, varName, "", irgen.GetBasicBlock());
				return varName;
			}
		}
		// Swizzle on left side
		if(dynamic_cast<FieldAccess*>(left)) {
			if(LSize == 1) {
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateMul(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				baseR = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
				for(int i = 0; i<LSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(baseR, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateMul(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
			}
		}
		// No swizzles
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateMul(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}
		/*if(dynamic_cast<FieldAccess*>(left)) {
			llvm::Value* lField = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
			llvm::Value *sum = llvm::BinaryOperator::CreateMul(lField, rVal, "", irgen.GetBasicBlock());
			baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
			return varName;
		}
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateMul(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}*/
	}
	if(!strcmp(op_, "/=")){
		// Swizzle on both sides
		if(dynamic_cast<FieldAccess*>(left) && dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				for(int i = 0; i<RSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(vecPtr, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
				return varName;
			}
		}
		// Swizzle on right side
		if(dynamic_cast<FieldAccess*>(right)) {
			if(RSize == 1) {
				rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
				return varName;
			}
			else {
				llvm::Value* vecPtr = new llvm::ShuffleVectorInst(baseR, llvm::UndefValue::get(baseR->getType()), mask, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(lVal, rVal, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(vecPtr, varName, "", irgen.GetBasicBlock());
				return varName;
			}
		}
		// Swizzle on left side
		if(dynamic_cast<FieldAccess*>(left)) {
			if(LSize == 1) {
				llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
				llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(left_val, rVal, "", irgen.GetBasicBlock());
				baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
				llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				return varName;
			}
			else {
				baseR = new llvm::LoadInst(rLoc, "", irgen.GetBasicBlock());
				for(int i = 0; i<LSize; i++) {
					llvm::Constant* id = llvm::ConstantInt::get(irgen.GetIntType(), i);
					rVal = llvm::ExtractElementInst::Create(baseR, id, "", irgen.GetBasicBlock());
					char letter = swizzleL[i];
					int idx = -1;
					if(letter == 'x') {
						idx = 0;
					}
					else if(letter == 'y') {
						idx = 1;
					}
					else if(letter == 'z') {
						idx = 2;
					}
					else if(letter == 'w') {
						idx = 3;
					}
					lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
					llvm::Value * left_val = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
					llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(left_val, rVal, "", irgen.GetBasicBlock());
					baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
					llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
				}
			}
		}
		// No swizzles
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}
		/*if(dynamic_cast<FieldAccess*>(left)) {
			llvm::Value* lField = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
			llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(lField, rVal, "", irgen.GetBasicBlock());
			baseL = llvm::InsertElementInst::Create(baseL, sum, lidxL, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(baseL, lVal, "", irgen.GetBasicBlock());
			return varName;
		}
		else {
			llvm::Value *sum = llvm::BinaryOperator::CreateSDiv(lVal, rVal, "", irgen.GetBasicBlock());
			llvm::StoreInst *ptr = new llvm::StoreInst(sum, varName, irgen.GetBasicBlock());
			return varName;
		}*/
	}

	return NULL;
}

llvm::Value* EqualityExpr::Emit() {

	llvm::Value* baseL = NULL;
	llvm::Value* baseR = NULL;
	char* swizzleL = NULL;
	char* swizzleR = NULL;
	llvm::Constant* lidxL = NULL;
	llvm::Constant* lidxR = NULL;

	llvm::Value* lVal = left->Emit();
	if(dynamic_cast<VarExpr*>(left) || dynamic_cast<AssignExpr*>(left)) {
		//if(!dynamic_cast<llvm::Argument*>(lVal))
			lVal = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(left)) {
		baseL = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
		swizzleL = dynamic_cast<FieldAccess*>(left)->getSwizzle();
		int idx = -1;
		if(!strcmp(swizzleL,"x"))
			idx = 0;
		else if(!strcmp(swizzleL,"y"))
			idx = 1;
		else if(!strcmp(swizzleL,"z"))
			idx = 2;
		else if(!strcmp(swizzleL,"w"))
			idx = 3;
		lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		lVal = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
	}
	llvm::Value* rVal = right->Emit();
	if(dynamic_cast<VarExpr*>(right) || dynamic_cast<AssignExpr*>(right)) {
		if(!dynamic_cast<llvm::Argument*>(rVal))
			rVal = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(right)) {
		baseR = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
		swizzleR = dynamic_cast<FieldAccess*>(right)->getSwizzle();
		int idx = -1;
		if(!strcmp(swizzleR,"x"))
			idx = 0;
		else if(!strcmp(swizzleR,"y"))
			idx = 1;
		else if(!strcmp(swizzleR,"z"))
			idx = 2;
		else if(!strcmp(swizzleR,"w"))
			idx = 3;
		lidxR = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
	}

	char* op_ = op->getOp();
	if(!strcmp(op_, "==")) {
		if(lVal->getType() == irgen.GetIntType() || lVal->getType() == irgen.GetBoolType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::ICmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::ICMP_EQ;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
		else if(lVal->getType() == irgen.GetFloatType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::FCmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::FCMP_OEQ;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
	}
	if(!strcmp(op_, "!=")) {
		if(lVal->getType() == irgen.GetIntType() || lVal->getType() == irgen.GetBoolType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::ICmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::ICMP_NE;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
		else if(lVal->getType() == irgen.GetFloatType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::FCmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::FCMP_ONE;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
	}
	return NULL;
}

llvm::Value* RelationalExpr::Emit() {
	llvm::Value* baseL = NULL;
	llvm::Value* baseR = NULL;
	char* swizzleL = NULL;
	char* swizzleR = NULL;
	llvm::Constant* lidxL = NULL;
	llvm::Constant* lidxR = NULL;

	llvm::Value* lVal = left->Emit();
	if(dynamic_cast<VarExpr*>(left) || dynamic_cast<AssignExpr*>(left)) {
		//if(!dynamic_cast<llvm::Argument*>(lVal))
			lVal = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(left)) {
		baseL = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
		swizzleL = dynamic_cast<FieldAccess*>(left)->getSwizzle();
		int idx = -1;
		if(!strcmp(swizzleL,"x"))
			idx = 0;
		else if(!strcmp(swizzleL,"y"))
			idx = 1;
		else if(!strcmp(swizzleL,"z"))
			idx = 2;
		else if(!strcmp(swizzleL,"w"))
			idx = 3;
		lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		lVal = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
	}
	llvm::Value* rVal = right->Emit();
	if(dynamic_cast<VarExpr*>(right) || dynamic_cast<AssignExpr*>(right)) {
		//if(!dynamic_cast<llvm::Argument*>(rVal))
			rVal = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(right)) {
		baseR = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
		swizzleR = dynamic_cast<FieldAccess*>(right)->getSwizzle();
		int idx = -1;
		if(!strcmp(swizzleR,"x"))
			idx = 0;
		else if(!strcmp(swizzleR,"y"))
			idx = 1;
		else if(!strcmp(swizzleR,"z"))
			idx = 2;
		else if(!strcmp(swizzleR,"w"))
			idx = 3;
		lidxR = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
	}

	char* op_ = op->getOp();
	if(!strcmp(op_, "<")) {
		if(lVal->getType() == irgen.GetIntType() || lVal->getType() == irgen.GetBoolType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::ICmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::ICMP_SLT;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
		else if(lVal->getType() == irgen.GetFloatType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::FCmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::FCMP_OLT;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
	}
	if(!strcmp(op_, "<=")) {
		if(lVal->getType() == irgen.GetIntType() || lVal->getType() == irgen.GetBoolType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::ICmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::ICMP_SLE;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
		else if(lVal->getType() == irgen.GetFloatType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::FCmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::FCMP_OLE;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
	}
	if(!strcmp(op_, ">")) {
		if(lVal->getType() == irgen.GetIntType() || lVal->getType() == irgen.GetBoolType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::ICmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::ICMP_SGT;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
		else if(lVal->getType() == irgen.GetFloatType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::FCmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::FCMP_OGT;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
	}
	if(!strcmp(op_, ">=")) {
		if(lVal->getType() == irgen.GetIntType() || lVal->getType() == irgen.GetBoolType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::ICmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::ICMP_SGE;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
		else if(lVal->getType() == irgen.GetFloatType()) {
			llvm::CmpInst::OtherOps llvmOP = llvm::CmpInst::FCmp;
			llvm::CmpInst::Predicate pred = llvm::ICmpInst::FCMP_OGE;
			llvm::Value *val = llvm::CmpInst::Create(llvmOP, pred, lVal, rVal, "", irgen.GetBasicBlock());
			return val;
		}
	}
	return NULL;
}

llvm::Value* LogicalExpr::Emit() {
	llvm::Value* baseL = NULL;
	llvm::Value* baseR = NULL;
	char* swizzleL = NULL;
	char* swizzleR = NULL;
	llvm::Constant* lidxL = NULL;
	llvm::Constant* lidxR = NULL;

	llvm::Value* lVal = left->Emit();
	if(dynamic_cast<VarExpr*>(left) || dynamic_cast<AssignExpr*>(left)) {
		//if(!dynamic_cast<llvm::Argument*>(lVal))
			lVal = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(left)) {
		baseL = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
		swizzleL = dynamic_cast<FieldAccess*>(left)->getSwizzle();
		int idx = -1;
		if(!strcmp(swizzleL,"x"))
			idx = 0;
		else if(!strcmp(swizzleL,"y"))
			idx = 1;
		else if(!strcmp(swizzleL,"z"))
			idx = 2;
		else if(!strcmp(swizzleL,"w"))
			idx = 3;
		lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		lVal = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
	}
	llvm::Value* rVal = right->Emit();
	if(dynamic_cast<VarExpr*>(right) || dynamic_cast<AssignExpr*>(right)) {
		if(!dynamic_cast<llvm::Argument*>(rVal))
			rVal = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(right)) {
		baseR = new llvm::LoadInst(rVal, "", irgen.GetBasicBlock());
		swizzleR = dynamic_cast<FieldAccess*>(right)->getSwizzle();
		int idx = -1;
		if(!strcmp(swizzleR,"x"))
			idx = 0;
		else if(!strcmp(swizzleR,"y"))
			idx = 1;
		else if(!strcmp(swizzleR,"z"))
			idx = 2;
		else if(!strcmp(swizzleR,"w"))
			idx = 3;
		lidxR = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		rVal = llvm::ExtractElementInst::Create(baseR, lidxR, "", irgen.GetBasicBlock());
	}

	char* op_ = op->getOp();
	if(!strcmp(op_, "&&")) {
		llvm::Value *ret = llvm::BinaryOperator::CreateAnd(lVal, rVal, "", irgen.GetBasicBlock());
		return ret;
	}
	if(!strcmp(op_, "||")) {
		llvm::Value *ret = llvm::BinaryOperator::CreateOr(lVal, rVal, "", irgen.GetBasicBlock());
		return ret;
	}
	return NULL;
}

llvm::Value* PostfixExpr::Emit() {
	llvm::Value* lVal = left->Emit();
	llvm::Value* loc = lVal;
	llvm::Value* baseL = NULL;
	char* swizzleL = NULL;
	llvm::Constant* lidxL = NULL;

	if(dynamic_cast<VarExpr*>(left) || dynamic_cast<AssignExpr*>(left)) {
		//if(!dynamic_cast<llvm::Argument*>(lVal))
			lVal = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
	}
	else if(dynamic_cast<FieldAccess*>(left)) {
		baseL = new llvm::LoadInst(lVal, "", irgen.GetBasicBlock());
		swizzleL = dynamic_cast<FieldAccess*>(left)->getSwizzle();
		int idx = -1;
		if(!strcmp(swizzleL,"x"))
			idx = 0;
		else if(!strcmp(swizzleL,"y"))
			idx = 1;
		else if(!strcmp(swizzleL,"z"))
			idx = 2;
		else if(!strcmp(swizzleL,"w"))
			idx = 3;
		lidxL = llvm::ConstantInt::get(irgen.GetIntType(), idx);
		lVal = llvm::ExtractElementInst::Create(baseL, lidxL, "", irgen.GetBasicBlock());
	}

	char* op_ = op->getOp();
	if(!strcmp(op_, "++")) {
		llvm::Value *val = llvm::ConstantInt::get(irgen.GetIntType(), 1);
		llvm::Value *sum = llvm::BinaryOperator::CreateAdd(lVal, val, "", irgen.GetBasicBlock());
		llvm::StoreInst *ptr = new llvm::StoreInst(sum, loc, irgen.GetBasicBlock());
		return lVal;
	}
	if(!strcmp(op_, "--")) {
		llvm::Value *val = llvm::ConstantInt::get(irgen.GetIntType(), 1);
		llvm::Value *sum = llvm::BinaryOperator::CreateSub(lVal, val, "", irgen.GetBasicBlock());
		llvm::StoreInst *ptr = new llvm::StoreInst(sum, loc, irgen.GetBasicBlock());
		return lVal;
	}

	return NULL;
}

llvm::Value* FieldAccess::Emit() {

	llvm::Value* addr = base->Emit();

	return addr;
}

llvm::Value* VarExpr::Emit() {
	extern List< std::map<char*, llvm::Value*, cmp_str>* >* scope_stack;
	std::map<char*,llvm::Value*, cmp_str>::const_iterator it;
	std::map<char*, llvm::Value*, cmp_str>* symTable = scope_stack->top();
	it = symTable->find(id->getName());
	int count = scope_stack->NumElements() - 2;
	while(it == symTable->end() && count >= 0) {
		symTable = scope_stack->Nth(count);
		it = symTable->find(id->getName());
		count = count-1;
	}

	if(it == symTable->end()) {
		llvm::Value* val = new llvm::GlobalVariable(irgen.GetIntType(),false,llvm::GlobalValue::LinkerPrivateLinkage);
		val->setName(id->getName());
		return val;
	}
	else {
		return it->second;
	}
	return NULL;
}

llvm::Value* IntConstant::Emit() {
	return llvm::ConstantInt::get(irgen.GetIntType(), value, true);
}

llvm::Value* FloatConstant::Emit() {
	return llvm::ConstantFP::get(irgen.GetFloatType(), value);
}

llvm::Value* BoolConstant::Emit() {
	if(value) {
		return llvm::ConstantInt::get(irgen.GetBoolType(), -1, true);
	}
	else {
		return llvm::ConstantInt::get(irgen.GetBoolType(), 0, true);
	}
}

IntConstant::IntConstant(yyltype loc, int val) : Expr(loc) {
    value = val;
}
void IntConstant::PrintChildren(int indentLevel) { 
    printf("%d", value);
}

FloatConstant::FloatConstant(yyltype loc, double val) : Expr(loc) {
    value = val;
}
void FloatConstant::PrintChildren(int indentLevel) { 
    printf("%g", value);
}

BoolConstant::BoolConstant(yyltype loc, bool val) : Expr(loc) {
    value = val;
}
void BoolConstant::PrintChildren(int indentLevel) { 
    printf("%s", value ? "true" : "false");
}

VarExpr::VarExpr(yyltype loc, Identifier *ident) : Expr(loc) {
    Assert(ident != NULL);
    this->id = ident;
}

void VarExpr::PrintChildren(int indentLevel) {
    id->Print(indentLevel+1);
}

Operator::Operator(yyltype loc, const char *tok) : Node(loc) {
    Assert(tok != NULL);
    strncpy(tokenString, tok, sizeof(tokenString));
}

void Operator::PrintChildren(int indentLevel) {
    printf("%s",tokenString);
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o, Expr *r) 
  : Expr(Join(l->GetLocation(), r->GetLocation())) {
    Assert(l != NULL && o != NULL && r != NULL);
    (op=o)->SetParent(this);
    (left=l)->SetParent(this); 
    (right=r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Operator *o, Expr *r) 
  : Expr(Join(o->GetLocation(), r->GetLocation())) {
    Assert(o != NULL && r != NULL);
    left = NULL; 
    (op=o)->SetParent(this);
    (right=r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o) 
  : Expr(Join(l->GetLocation(), o->GetLocation())) {
    Assert(l != NULL && o != NULL);
    (left=l)->SetParent(this);
    (op=o)->SetParent(this);
}

void CompoundExpr::PrintChildren(int indentLevel) {
   if (left) left->Print(indentLevel+1);
   op->Print(indentLevel+1);
   if (right) right->Print(indentLevel+1);
}
   
  
ArrayAccess::ArrayAccess(yyltype loc, Expr *b, Expr *s) : LValue(loc) {
    (base=b)->SetParent(this); 
    (subscript=s)->SetParent(this);
}

void ArrayAccess::PrintChildren(int indentLevel) {
    base->Print(indentLevel+1);
    subscript->Print(indentLevel+1, "(subscript) ");
  }
     
FieldAccess::FieldAccess(Expr *b, Identifier *f) 
  : LValue(b? Join(b->GetLocation(), f->GetLocation()) : *f->GetLocation()) {
    Assert(f != NULL); // b can be be NULL (just means no explicit base)
    base = b; 
    if (base) base->SetParent(this); 
    (field=f)->SetParent(this);
}


  void FieldAccess::PrintChildren(int indentLevel) {
    if (base) base->Print(indentLevel+1);
    field->Print(indentLevel+1);
  }

Call::Call(yyltype loc, Expr *b, Identifier *f, List<Expr*> *a) : Expr(loc)  {
    Assert(f != NULL && a != NULL); // b can be be NULL (just means no explicit base)
    base = b;
    if (base) base->SetParent(this);
    (field=f)->SetParent(this);
    (actuals=a)->SetParentAll(this);
}

 void Call::PrintChildren(int indentLevel) {
    if (base) base->Print(indentLevel+1);
    if (field) field->Print(indentLevel+1);
    if (actuals) actuals->PrintAll(indentLevel+1, "(actuals) ");
  }
 
