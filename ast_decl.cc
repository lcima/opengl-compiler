/* File: ast_decl.cc
 * -----------------
 * Implementation of Decl node classes.
 */
#include "ast_decl.h"
#include "ast_type.h"
#include "ast_stmt.h"
#include "irgen.h"
   
struct cmp_str
{
   bool operator()(char const *a, char const *b)
   {
      return std::strcmp(a, b) < 0;
   }
};
      
Decl::Decl(Identifier *n) : Node(*n->GetLocation()) {
    Assert(n != NULL);
    (id=n)->SetParent(this); 
}


VarDecl::VarDecl(Identifier *n, Type *t) : Decl(n) {
    Assert(n != NULL && t != NULL);
    name = n;
    (type=t)->SetParent(this);
}
  
void VarDecl::PrintChildren(int indentLevel) { 
   if (type) type->Print(indentLevel+1);
   if (id) id->Print(indentLevel+1);
}

llvm::Value* VarDecl::Emit() {
   extern List< std::map<char*, llvm::Value*, cmp_str>* >* scope_stack;
   llvm::Type* var = type->getType();
   char* n = name->getName();
   llvm::Module *mod = irgen.GetOrCreateModule("foo.bc");
   if(irgen.GetBasicBlock() == NULL) {
fprintf(stderr, "adding global %s\n", n);
	llvm::Value* alloc = new llvm::GlobalVariable(*irgen.GetOrCreateModule("foo.bc"), var, false, llvm::GlobalValue::ExternalLinkage, llvm::Constant::getNullValue(var), n);
	scope_stack->top()->insert(std::pair<char*,llvm::Value*>(n,alloc));
	return alloc;
      /*mod->getOrInsertGlobal(n,var);
      scope_stack->top()->insert(std::pair<char*,llvm::Value*>(n,NULL));*/
   }
   else {
fprintf(stderr, "adding local %s\n", n);
	llvm::Value* alloc = new llvm::AllocaInst(var, n,irgen.GetBasicBlock());
	scope_stack->top()->insert(std::pair<char*,llvm::Value*>(n,alloc));
	return alloc;
   }
}

llvm::Value* FnDecl::Emit() {
    extern List< std::map<char*, llvm::Value*, cmp_str>* >* scope_stack;
    llvm::Module *mod = irgen.GetOrCreateModule("foo.bc");
    std::vector<llvm::Type *> argTypes;
    for(int i = 0; i < formals->NumElements(); i++) {
        VarDecl* v = formals->Nth(i);
        llvm::Type* t = v->getType()->getType();
        argTypes.push_back(t);
    }
    llvm::Type* retTy = returnType->getType();
    llvm::ArrayRef<llvm::Type *> argArray(argTypes);
    llvm::FunctionType *funcTy = llvm::FunctionType::get(retTy, argArray, false);
    llvm::Function *f = llvm::cast<llvm::Function>(mod->getOrInsertFunction(name->getName(), funcTy));
    llvm::LLVMContext *context = irgen.GetContext();
    llvm::BasicBlock *bb = llvm::BasicBlock::Create(*context, "entry", f);
    irgen.SetBasicBlock(bb);
    int count = 0;
    std::string argStr = "arg";
    for(llvm::Function::arg_iterator it = f->arg_begin(); it != f->arg_end(); it++) {
        VarDecl* v = formals->Nth(count);
        llvm::Argument *arg = it;
        char* n = v->getName();
	llvm::Type* var = v->getType()->getType();
	char numstr[21]; // enough to hold all numbers up to 64-bits
	sprintf(numstr, "%d", count);
        arg->setName(argStr + numstr);
	llvm::Value* alloc = new llvm::AllocaInst(var, n,irgen.GetBasicBlock());
	llvm::StoreInst *ptr = new llvm::StoreInst(arg, alloc, irgen.GetBasicBlock());
	scope_stack->top()->insert(std::pair<char*,llvm::Value*>(n,alloc));
        count++;
    }

    irgen.SetFunction(f);
    body->Emit();
}

FnDecl::FnDecl(Identifier *n, Type *r, List<VarDecl*> *d) : Decl(n) {
    Assert(n != NULL && r!= NULL && d != NULL);
    name = n;
    (returnType=r)->SetParent(this);
    (formals=d)->SetParentAll(this);
    body = NULL;
}

void FnDecl::SetFunctionBody(Stmt *b) { 
    (body=b)->SetParent(this);
}

void FnDecl::PrintChildren(int indentLevel) {
    if (returnType) returnType->Print(indentLevel+1, "(return type) ");
    if (id) id->Print(indentLevel+1);
    if (formals) formals->PrintAll(indentLevel+1, "(formals) ");
    if (body) body->Print(indentLevel+1, "(body) ");
}


